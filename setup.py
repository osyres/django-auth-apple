from setuptools import setup

setup(
    install_requires=[
        'Django',
        'djangorestframework',
        'PyJWT==2.4.0',
        'requests==2.27.1',
    ]
)
